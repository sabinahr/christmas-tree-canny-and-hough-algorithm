import cv2

# Load a color image
image = cv2.imread("brad.png", 1)

# Convert RGB image to Gray
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# Display the color game
cv2.imshow('color_image', image)

# Display the gray image
cv2.imshow('gray_image', gray)

cv2.waitKey(0)
cv2.distroyAllWindows()