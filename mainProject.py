import cv2
import numpy
import math
from skimage.measure import label
from scipy.ndimage import maximum_filter
import collections

colors = {'white' : (255,255,255), 'green' : (0,85,60), 'brown' : (0,0,60), 'yellow' : (0,200,200), 'red' : (0,0,200)}

def main():
	global height, width

	grayscale_img = cv2.imread('brad.png', cv2.IMREAD_GRAYSCALE)

	height, width = grayscale_img.shape[:2]
	
	canny_img, non_max_supression_img = canny(grayscale_img)
	colored_img = floodFill(canny_img)
	hough(colored_img, non_max_supression_img)

	cv2.waitKey(0)
	print('Done :)')
	cv2.destroyAllWindows()

def canny(grayscale_img):
	print("In canny function :")
	global height, width
	pixels_grad, pixels_grad_dir = sobel(grayscale_img)
	non_max_supression_img = non_max_supression(pixels_grad, pixels_grad_dir)
	
	cv2.imshow('pixels_grad', pixels_grad)
	cv2.imshow('pixels_grad_dir', pixels_grad_dir)

	cv2.imshow('non_max_supression_img', non_max_supression_img)

	return pixels_grad, non_max_supression_img

def sobel(image):
	print('	- In sobel function')
	pixels_grad = numpy.zeros((height, width, 1), numpy.uint8)
	pixels_grad_dir = numpy.zeros((height, width, 1), numpy.uint8)

	sobelX = [[-1, 0, 1],\
			[-2, 0, 2],\
			[-1, 0, 1]]
		  
	sobelY = [[1, 2, 1],\
			[0, 0, 0],\
			[-1, -2, -1]]

	for x in range(1, height-1):
		for y in range(1, width-1):
			gradx = grady = 0
			# inmultesc kernelul cu o regiune din matrice 3x3 si adun valorile din matricea rez 
			gradx = (sobelX * image[x-1:x+2,y-1:y+2]).sum()
			grady = (sobelY * image[x-1:x+2,y-1:y+2]).sum()
			# radical x^2 + y^2
			pixels_grad[x, y] = numpy.hypot(gradx,grady)

			# gradient direction
			theta = numpy.rad2deg(math.atan2(grady, gradx)) % 180

			if (theta >= 0 and theta < 22.5) or (theta >= 157.5 and theta <= 180):
				graddir = 0
			elif theta >= 22.5 and theta < 67.5:
				graddir = 45
			elif theta >= 67.5 and theta < 112.5:
				graddir = 90
			elif theta >= 112.5 and theta < 157.5:
				graddir = 135
			else:
				graddir = 0
			# store gradient direction
			pixels_grad_dir[x,y] = graddir

	return pixels_grad, pixels_grad_dir

def non_max_supression(pixels_grad, pixels_grad_dir):
	print('	- In non_max_supression function')
	sensitivity = 4
	pixels_nm = numpy.ones((height, width, 1), numpy.uint8)

	for x in range(1, height - 1):
		for y in range(1, width - 1):
			if (pixels_grad[x, y] >= sensitivity):
				grad_dir = pixels_grad_dir[x,y]
				if (grad_dir == 0):
					if (pixels_grad[x, y] > pixels_grad[x, y-1] and \
						pixels_grad[x, y] >= pixels_grad[x, y+1]):
						# mark current pixel as maximal
						pixels_nm[x, y] = 255
						# non-maximal
						pixels_nm[x, y-1] = 0
						pixels_nm[x, y+1] = 0
					else:
						# mark current pixel as non-maximal
						pixels_nm[x, y] = 0

				elif (grad_dir == 45):
					if (pixels_grad[x, y] > pixels_grad[x-1, y-1] and\
						pixels_grad[x, y] >= pixels_grad[x+1, y+1]):
						pixels_nm[x,y] = 255

						pixels_nm[x+1, y-1] = 0
						pixels_nm[x-1, y+1] = 0
					else:
						pixels_nm[x, y] = 0

				elif (grad_dir == 90):
					if (pixels_grad[x, y] > pixels_grad[x-1, y] and \
						pixels_grad[x, y] >= pixels_grad[x+1, y]):
						pixels_nm[x, y] = 255

						pixels_nm[x-1, y] = 0
						pixels_nm[x+1, y] = 0
					else:
						pixels_nm[x, y] = 0

				elif (grad_dir == 135):
					if (pixels_grad[x, y] > pixels_grad[x-1, y+1] and\
						pixels_grad[x, y] >= pixels_grad[x+1, y-1]):
						pixels_nm[x, y] = 255

						pixels_nm[x-1, y-1] = 0
						pixels_nm[x+1, y+1] = 0
					else:
						pixels_nm[x, y] = 0
			else:
				pixels_nm[x, y] = 0
	return  pixels_nm

def floodFill(image):
	print("In floodFill function")

	image = image.reshape((image.shape[0],image.shape[1]))

	output_img = numpy.zeros((height, width, 3), dtype=numpy.uint8)
	intermediate_pic = numpy.zeros((height, width), dtype=numpy.uint8)
	# compara fiecare elem, verif daca e negru si imi da o matr de bool
	edges = (image == 0)
	intermediate_pic[edges] = 1
	cv2.imshow("aaaaaaaa-intermediate_pic",intermediate_pic* 100)

	# separa in regiuni tot ce e diferit de background
	# micsorez poza pentru ca am un pixel 0
	intermediate_pic[2:height-1,2:width-1] = label(intermediate_pic[2:height-1, 2:width-1],background = 0)

	to_fill = intermediate_pic == 0 

	f2 = numpy.ones((5,5), dtype=numpy.uint8)

	# pt fiecare elem, fac max dintre vecini
	max_els = maximum_filter(intermediate_pic, footprint=f2, mode='constant', cval=-numpy.inf)
	# in zonele goale, pun elem maxim din jur
	intermediate_pic[to_fill] = max_els[to_fill]

	nr_regions = numpy.max(intermediate_pic)
	list_regions = []

	for i in range(nr_regions + 1):
		dim_region = numpy.count_nonzero(intermediate_pic == i)
		list_regions.append((i, dim_region))

	list_regions = sorted(list_regions, key=lambda x : x[1], reverse=True)

	# colorarea regiunilor
	mask = intermediate_pic == list_regions[0][0]
	output_img[mask] = colors['white']

	mask = intermediate_pic == list_regions[1][0]
	output_img[mask] = colors['green']
	
	mask = intermediate_pic == list_regions[2][0]
	output_img[mask] = colors['brown']

	for i in range(3, len(list_regions)):
		mask = intermediate_pic == list_regions[i][0]
		output_img[mask] = colors['yellow']

	cv2.imshow("Regiunile", intermediate_pic.astype('uint8') * 10)
	cv2.imshow("Colored pic without hough", output_img.astype('uint8'))

	return output_img

def hough(original_image, img):
	print("In hough function")
	accumulate_array = numpy.zeros((height, width, 60), dtype=numpy.uint8)
	
	edges = numpy.where(img >= 25)
	range_div = numpy.arange(0,2*numpy.pi, numpy.pi/8)

	for i in range(len(edges[0])):
		# pt fiecare raz aproximezi punctele de pe cerc
		for radius in range(35, 60):
			y_edge = edges[0][i]
			x_edge = edges[1][i]
			if x_edge <= radius or \
				x_edge >= width - radius or \
				y_edge <= radius or \
				y_edge >= height - radius:
				continue
			for angle in range_div:
				x = int(x_edge + radius * numpy.cos(angle))
				y = int(y_edge + radius * numpy.sin(angle))
				accumulate_array[y, x, radius] += 1

	acc_w = accumulate_array.shape[0]
	acc_h = accumulate_array.shape[1]
	acc_nr_raze_cercuri = accumulate_array.shape[2]
	
	posible_circles = accumulate_array.reshape(acc_w * acc_h * acc_nr_raze_cercuri)
	s_posible_circles = posible_circles.argsort()[::-1][:10]
	circles = []

	for q in s_posible_circles:
		y = q / (acc_h * acc_nr_raze_cercuri)
		q = q % (acc_h * acc_nr_raze_cercuri)
		x = q / acc_nr_raze_cercuri
		radius = q % acc_nr_raze_cercuri
		
		boolean = 0
		for c in circles:
			if (c[0] - y)* (c[0] - y) + (c[1]-x)* (c[1]-x) < radius* radius:
				boolean = 1
		
		# centrare cerc si raza
		if boolean == 0:
			adaos_raza = 2
			if radius == 45:
				x -= 3
				y += 2
				adaos_raza = 3
			if radius == 55:
				x += 3
				y += 3	
				adaos_raza = 4

			circles.append((y, x, radius))
			cv2.circle(original_image, (int(x), int(y)), radius+adaos_raza, colors['red'], -1)
		
		if len(circles) == 3:
			break

	cv2.imshow('DONE HOMEWORK', original_image)

if __name__ == "__main__":
	main()